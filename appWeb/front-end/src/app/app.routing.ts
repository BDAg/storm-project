import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from "@angular/core";
import { OfertasComponent } from './pages/ofertas/ofertas.component';
import { OfertaComponent } from './pages/oferta/oferta.component';

const routes: Routes = [
    { path: 'ofertas', component: OfertasComponent },
    { path: "oferta/:_id", component: OfertaComponent }
];

export const routingModule: ModuleWithProviders = RouterModule.forRoot(routes);